# Use the published docker image


```
usage: main.py [-h] [--url URL] [--chunk_size CHUNK_SIZE] [--create_index]
               index total glob

Index ais topic to ElasticSearch

positional arguments:
  index                 index of the indexing process amongst all the indexing
                        processes
  total                 total number of indexing processes
  glob                  input files to parse in a glob format

optional arguments:
  -h, --help            show this help message and exit
  --url URL, -u URL     Elasticsearch url
  --chunk_size CHUNK_SIZE, -c CHUNK_SIZE
                        Chunk size for Elasticsearch
  --create_index        If the index should be created
```

Each indexing process will take 1 core CPU.

If you need to run N instances in parallel you'll need to supply the following values for `index, total`: 
```
index total
0 N
1 N
2 N
...
N-1 N
```
## Delete the index

```
http --auth elastic:changeme DELETE http://192.168.143.244:9200/bulk-ais-2017.03
```

## vagrant setup


```
# Single indexion instance
docker run\
    -e PYTHONUNBUFFERED=1\
    -v /Users/msimonin/sed/sesame/platform/libs/../orbcomm:/orbcomm msimonin/sesame_indexion_bulk:latest\
    --url "192.168.143.245:9200" 1 1 "/orbcomm/aivdm/2017/03/01/*
```

## Example on G5K

This will start 32 indexion processes using one index and 3 shards

```
docker service create \
    -e PYTHONUNBUFFERED=1 \
    -e SESAME_UID=<sesame_uid> \
    -e SESAME_GID=<sesame_gid> \
    --restart-condition=none \
    --network host \
    --mount type=volume,volume-opt=o=addr=srv-bigdata.rennes.grid5000.fr,volume-opt=device=:/srv/bigdata,volume-opt=type=nfs,source=bigdata,target=/bigdata \
    --name=indexion-0 \
    msimonin/sesame_indexion_bulk:latest \
    --url node1:9200 node2:9200 node3:9200  --create_index --shards 3 --chunk_size 2000 -- 0 32 /bigdata/groups/sesame/orbcomm/aivdm/2017/03/*/*
```

```
for i in {1..31}
do
docker service create \
    -e PYTHONUNBUFFERED=1 \
    -e SESAME_UID=<sesame_uid> \
    -e SESAME_GID=<sesame_gid> \
    --restart-condition=none \
    --network host \
    --mount type=volume,volume-opt=o=addr=srv-bigdata.rennes.grid5000.fr,volume-opt=device=:/srv/bigdata,volume-opt=type=nfs,source=bigdata,target=/bigdata \
    --name=indexion-$i \
    msimonin/sesame_indexion_bulk:latest \
    --url chetemi-13.lille.grid5000.fr:9200 chetemi-14.lille.grid5000.fr:9200 chetemi-15.lille.grid5000.fr:9200  --chunk_size 2000 -- $i 32 /bigdata/groups/sesame/orbcomm/aivdm/2017/03/*/*; done
```
