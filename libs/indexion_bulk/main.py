#!/usr/bin/env python
"""
Index data to elasticsearch

Input: files made of binary ais
Output: ais in json indexed in elasticsearch
"""
import argparse
import ais.stream
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
import glob

INDEX_NAME = "bulk-ais-2017.03"
TYPE_NAME = "bulk-ais_dynamic"

PROPERTIES = {
    #"assigned_mode": {"type": "boolean"},
    "mmsi": {"type": "keyword"},
    "nav_status": {"type": "text"},
    "position_accuracy": {"type": "text"},
    "raim": {"type": "text"},
    "repeat_indicator": {"type": "text"},
    "rot": {"type": "double"},
    "rot_over_range": {"type": "text"},
    "slot_offset": {"type": "text"},
    "slot_timeout": {"type": "text"},
    "sog": {"type": "double"},
    "special_manoeuvre": {"type": "text"},
    "true_heading": {"type": "double"},
    "x": {"type": "double"},
    "y": {"type": "double"},
    "tagblock_timestamp": {"type": "text"},
    # generated ones
    "location": {"type": "geo_point"},
    "timestamp": {"type": "date"},
}




# We'll remove any extra field
authorized_fields = PROPERTIES.keys()

# Let's write a generator
def gen_ais(input_file, index_name, type_name):
    with open(input_file) as f:
        for ais_msg in ais.stream.decode(f):
            msg = {}
            if not ais_msg.get("x"):
                continue

            # remove reserved keyword if any
            for k in ais_msg.keys():
                if k in list(authorized_fields):
                    msg[k] = ais_msg[k]
            # Let's correct some lat/lon mistakes ...
            # and save the original ones
            if msg["x"] > 180 or msg["x"] < -180 :
                msg["_x_old"] = msg["x"]
                msg["x"] = msg["x"] % 180
            if msg["y"] > 90 or msg["y"] < -90 :
                msg["_y_old"] = msg["y"]
                msg["y"] = msg["y"] % 90

            msg.update({
                "_index": index_name,
                "_type": type_name,
                "location": [msg.get("x"), msg.get("y")],
                # Getting the timestamp in the right format
                "timestamp": 1000 * msg["tagblock_timestamp"]
            })
            yield msg

def streaming_bulk_index(es, files, index_name, type_name, chunk_size):
    i = 0
    for input_file in files:
        for ok, result in streaming_bulk(
                            es,
                            gen_ais(input_file, index_name, type_name),
                            chunk_size=chunk_size):

            action, result = result.popitem()
            doc_id = '/%s/%s/%s' % (index_name, type_name, result['_id'])
            # process the information from ES whether the document has been
            # successfully indexed
            if not ok:
                print('Failed to %s document %s: %r' % (action, doc_id, result))
            i = i + 1
            if not i % chunk_size :
                print("Indexed %i docs" % i)


if __name__  == "__main__":
    index_name = INDEX_NAME
    type_name = TYPE_NAME

    parser = argparse.ArgumentParser(description="Index ais topic to ElasticSearch")
    parser.add_argument('--url', '-u',
                        nargs="+",
                        help="Elasticsearch url(s)",
                        default='localhost:9200')

    parser.add_argument('--chunk_size', '-c', type=int,
                        help="Chunk size for Elasticsearch",
                        default=500)

    parser.add_argument('--create_index',
                       help="If the index should be created",
                        action='store_true')

    parser.add_argument('--shards', type=int,
                        help="Number of shards to use (when create_index is set)",
                        default=5)

    parser.add_argument('--replicas', type=int,
                        help="Number of replicas to use (when create_index is set)",
                        default=0)

    parser.add_argument("index", type=int,
                        help="index of the indexing process amongst all the indexing processes")

    parser.add_argument("total", type=int,
                        help="total number of indexing processes")

    parser.add_argument("glob", type=str,
                        help="input files to parse in a glob format")
    args = parser.parse_args()
    print(args)

    chunk_size = args.chunk_size
    source_glob = args.glob
    es = Elasticsearch(args.url, timeout=60)
    # Some preliminary steps
    # TODO(msimonin): check if it exists
    # For now you can simply launch
    # http --auth elastic:changeme DELETE http://192.168.143.244:9200/bulk-ais-2017.03
    # If you want to reindex everything from scratch
    BODY = {
        "settings": {
            "number_of_shards" : args.shards,
            "number_of_replicas" : args.replicas,
            # Some indexing performance setting
            "translog": {
                "sync_interval": "10s",
                "durability": "async"
            },
            "refresh_interval" : "30s"
        },
        "mappings": {
            TYPE_NAME: {
                "properties": PROPERTIES
            }
        }
    }

    if args.create_index:
        es.indices.create(index=index_name, body=BODY)

    files =  sorted(glob.glob(source_glob))
    total_files = len(files)
    ratio = int(total_files / args.total)
    start = int(ratio * args.index)
    end = min(start + ratio, len(files))
    files = files[start:end]
    print("Files to parse : %s" % files)
    print("Number of files to parse : %s" % len(files))
    streaming_bulk_index(es, files, index_name, type_name, chunk_size)
