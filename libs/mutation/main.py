#!/usr/bin/env python
"""
Mutate the original AIS with some minimal pre-computed informations.
"""

import argparse
import asyncio
import functools
import concurrent.futures
import logging
import math
import pickle
import sys

from sesamelib.geometry import distance
from sesamelib.sesamelib_faust import MutatedDynamicMessage

from utils import _mutate

import faust


APP_NAME = "mutation"

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Mutate the ais dynamic signal")
    parser.add_argument("--bootstrap_servers", "-b",
                        help="Kafka bootstrap servers",
                        action="append",
                        required=True)
    args = parser.parse_args()

    obj = None
    with open("data/index-0.5.pickle", "br") as f:
            obj = pickle.load(f)

    index = obj["index"]
    eezs = obj["eezs"]
    cell_size = obj["cell_size"]

    loop = asyncio.get_event_loop()

    pool = concurrent.futures.ThreadPoolExecutor()

    app = faust.App(
        APP_NAME,
        loop=loop,
        broker="kafka://{}".format(",".join(args.bootstrap_servers))
    )

    topic = app.topic("ais.dynamic",
                      key_type=str,
                      value_type=MutatedDynamicMessage)
    out_topic = app.topic("ais.dynamic.mutated",
                          key_type=str,
                          value_type=MutatedDynamicMessage)

    pmsgs = {}

    @app.agent(topic)
    async def mutate_all(msgs):
        async for msg in msgs:
            zone_distance = sys.maxsize
            zone_mrgid = ""
            # Beware of mixing x,y
            x = msg.y
            y = msg.x
            x1, y1 = ((math.floor(x / cell_size) * cell_size + 90) % 180 - 90,
                     (math.floor(y / cell_size) * cell_size + 180) % 360 - 180)

            x2, y2 = ((math.ceil(x / cell_size) * cell_size + 90) % 180 - 90,
                     (math.ceil(y / cell_size) * cell_size + 180) % 360 - 180)

            mrgids_to_test = set()
            mrgids_to_test = mrgids_to_test.union(set(index[x1, y1]))\
                                           .union(set(index[x1, y2]))\
                                           .union(set(index[x2, y2]))\
                                           .union(set(index[x2, y1]))
            logging.debug("%s, %s -> %s" % (x, y, mrgids_to_test))
            for mrgid in mrgids_to_test:
                # I guess we could even parallelize this
                d = await loop.run_in_executor(pool,
                                               functools.partial(distance,
                                                                 x,
                                                                 y,
                                                                 eezs[mrgid]))
                if d < zone_distance:
                    zone_mrgid = mrgid
                    zone_distance = zone_distance
                    zone_distance = d

            mtm = _mutate(msg=msg, pmsgs=pmsgs, grid_x=x1, grid_y=x2,
                          zone_mrgid=zone_mrgid, zone_distance=zone_distance)
            # finally emit the mutated message
            # NOTE(msimonin): Surprinsingly we need to force the type to str
            # here
            await out_topic.send(key=str(mtm.mmsi), value=mtm)

    app.finalize()
    worker = faust.Worker(app,
                          loglevel="INFO",
                          loop=loop)
    worker.execute_from_commandline()
    pool.shutdown()
