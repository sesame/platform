import collections
import copy
from datetime import datetime

from sesamelib.sesame_faust import BaseDynamicMessage, MutatedDynamicMessage


HISTORY_SIZE = 5

def _mutate(*, msg=None, pmsgs=None, grid_x=None, grid_y=0.0, zone_mrgid=None,
            zone_distance=None):
    """This returns a mutated message given some calculated inputs. It also
    update the list of previous seen messages (up to HISTORY_SIZE).

    Example:
    - For the first messages: dx, dy, dt will be [] (difference with
      itself).
    - For the subsequent messages: dx, dy, dt must be the real difference with
      msg_{n-1}, msg_{n-2} ...
    """

    mtm = MutatedDynamicMessage.fromBasicMessage(msg)
    mtm.timestamp = 1000 * msg.tagblock_timestamp
    mtm.grid_x = grid_x
    mtm.grid_y = grid_y
    mtm.human_timestamp = datetime.utcfromtimestamp(
        mtm.tagblock_timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')

    pmsgs.setdefault(msg.mmsi, collections.deque([], HISTORY_SIZE))

    # Get the previous messages
    dmsgs = copy.deepcopy(pmsgs[msg.mmsi])

    # Update the windows
    pmsgs[msg.mmsi].appendleft(mtm)

    mtm.zone_mrgid = zone_mrgid
    mtm.zone_distance = zone_distance

    if len(dmsgs) == 0:
        return mtm

    # We populate the windowed speed
    for pmsg in dmsgs:
        mtm.dx.append(mtm.x - pmsg.x)
        mtm.dy.append(mtm.y - pmsg.y)
        # dt in seconds
        mtm.dt.append(mtm.tagblock_timestamp - pmsg.tagblock_timestamp)

    return mtm
