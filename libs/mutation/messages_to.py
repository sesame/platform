#!/usr/bin/env python

from sesamelib.geometry import load_geometry

import json
import numpy as np
import os
import time
import sys


HERE = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))


if __name__ == "__main__":
    file_path = os.path.join(HERE, "data/{}.xml".format(sys.argv[1]))
    # generate messages that can be consumed by kafka producer
    # e.g:

    geometry = load_geometry(file_path)
    geom = geometry["geometry"]
    x1, x2, y1, y2 = geom.GetEnvelope()

    # Make data.
    X = np.linspace(x1, x2, 100)
    Y = np.linspace(y1, y2, 100)

    current = 0.0
    timestamp = 1488326400

    for idx, x in enumerate(X):
        for y in Y:
            current = time.time()
            msg = {"mmsi": "1234",
                # Ais message have this weirdness to invert x and y
                "x": y,
                "y": x,
                "tagblock_timestamp": current,
                "sog": current,
                "true_heading": current,
                }
            current = current + 1
            print("{}|{}".format(int(current), json.dumps(msg)))
            time.sleep(0)
