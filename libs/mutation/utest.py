from utils import _mutate, HISTORY_SIZE
from sesamelib import faust
import unittest

class TestMutation(unittest.TestCase):

    def test_mutate_2_messages(self):
        pmsgs = {}
        zone_mrgid = "4321"
        zone_distance = 0.0
        grid_x = 0.0
        grid_y = 0.0
        msg = faust.BaseDynamicMessage(x=0.0,
                                       y=0.0,
                                       mmsi="1234",
                                       tagblock_timestamp=0,
                                       true_heading=0.0,
                                       sog=0.0)

        mtm = _mutate(msg=msg,
                      pmsgs=pmsgs,
                      grid_x=grid_x,
                      grid_y=grid_y,
                      zone_mrgid=zone_mrgid,
                      zone_distance=zone_distance)

        self.assertEqual(mtm, list(pmsgs[msg.mmsi])[0])
        self.assertEqual([], mtm.dx)
        self.assertEqual([], mtm.dy)
        self.assertEqual([], mtm.dt)
        self.assertEqual(grid_x, mtm.grid_x)
        self.assertEqual(grid_y, mtm.grid_y)
        self.assertEqual(zone_distance, mtm.zone_distance)

        msg = faust.BaseDynamicMessage(x=1.0,
                                       y=2.0,
                                       mmsi="1234",
                                       tagblock_timestamp=3.0,
                                       true_heading=0.0,
                                       sog=0.0)

        mtm = _mutate(msg=msg,
                      pmsgs=pmsgs,
                      grid_x=grid_x,
                      grid_y=grid_y,
                      zone_mrgid=zone_mrgid,
                      zone_distance=zone_distance)

        self.assertEqual(2, len(pmsgs[msg.mmsi]))
        self.assertEqual(mtm, list(pmsgs[msg.mmsi])[0])
        self.assertEqual([1.0], mtm.dx)
        self.assertEqual([2.0], mtm.dy)
        self.assertEqual([3.0], mtm.dt)
        self.assertEqual(grid_x, mtm.grid_x)

    def test_mutate_n_messages(self):
        pmsgs = {}
        zone_mrgid = "4321"
        zone_distance = 0.0
        grid_x = 0.0
        grid_y = 0.0

        mtm = None
        for i in range(100):
            msg = faust.BaseDynamicMessage(x=0.0,
                                        y=0.0,
                                        mmsi="1234",
                                        tagblock_timestamp=0,
                                        true_heading=0.0,
                                        sog=0.0)

            mtm = _mutate(msg=msg,
                        pmsgs=pmsgs,
                        grid_x=grid_x,
                        grid_y=grid_y,
                        zone_mrgid=zone_mrgid,
                        zone_distance=zone_distance)

        self.assertEqual(HISTORY_SIZE, len(pmsgs[msg.mmsi]))
        self.assertEqual(HISTORY_SIZE, len(mtm.dx))
        self.assertEqual(HISTORY_SIZE, len(mtm.dy))
        self.assertEqual(HISTORY_SIZE, len(mtm.dt))


if __name__ == "__main__":
    unittest.main()
