# Useful commands

-- adapt them  to your environment

```
bin/kafka-topics.sh  --zookeeper localhost:2181 --delete --topic ais.dynamic
python ~/workspace/repos/platform/libs/alert01/messages.py 5677 | bin/kafka-console-producer.sh  --broker-list localhost:9092 --topic ais.dynamic --property parse.key=true --property key.separator=\|
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ais.dynamic --property print.key=true --from-beginning

docker run --network host -ti msimonin/sesame_alert01-all --bootstrap_servers localhost:9092
```
