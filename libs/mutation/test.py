import faust

class BaseDynamicMessage(faust.Record):
    x: float
    y: float
    mmsi: str
    tagblock_timestamp: int
    timestamp: int
    true_heading: float
    sog: float


class MutatedDynamicMessage(BaseDynamicMessage):
    previous: BaseDynamicMessage = None

    v0x: float = None
    ## (y-py)/Dt or None
    v0y: float = None
    ## closest zone
    zone_mrgid: str = None
    zone_distance: float = None

    @classmethod
    def fromBasicMessage(cls, msg):
        return cls(x=msg.x,
                   y=msg.y,
                   mmsi=msg.mmsi,
                   tagblock_timestamp=msg.tagblock_timestamp,
                   timestamp=msg.timestamp,
                   true_heading=msg.true_heading,
                   sog=msg.sog)

bdm = BaseDynamicMessage(x=1, y=1, mmsi="", tagblock_timestamp=1234, true_heading=0.0, sog=0.1, timestamp=124)
mdm = MutatedDynamicMessage(x=1, y=1, mmsi="", tagblock_timestamp=1234, true_heading=0.0, sog=0.1, timestamp=124,
                            previous=bdm)

mdm2 = MutatedDynamicMessage.fromBasicMessage(bdm)
mdm2.v0x = 1234.5

print(mdm)
print(mdm2)

