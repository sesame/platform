#!/usr/bin/env python
"""
Ingest data to Kafka.

Input: files made of binary ais
Output: ais in json serialized form in ais topic
"""
import argparse
import json
import glob
import logging
import time

from kafka import KafkaProducer
import sesamelib.ais_utils as ais_utils


TOPIC_STATIC = "ais.static"
TOPIC_DYNAMIC = "ais.dynamic"

DYNAMIC_KEY = ["mmsi", "x", "y", "tagblock_timestamp", "true_heading", "sog", "cog", "rot"]
STATIC_KEY = ["mmsi", "destination", "tagblock_timestamp"]


def clean(msg, keys):
    _msg = {}
    for k in keys:
        _msg[k] = msg.get(k)
    return _msg

def fix_pos(msg):
    ais = {}
    if msg["x"] > 180 or msg["x"] < -180 :
        ais["x"] = msg["x"] % 180
    if msg["y"] > 90 or msg["y"] < -90 :
        ais["y"] = msg["y"] % 90
    return ais


if __name__ == "__main__":

    parser = argparse.ArgumentParser("Ingest data to Kafka ais topic")
    parser.add_argument("--bootstrap_servers", "-b",
                        help="Kafka bootstrap servers",
                        action="append",
                        required=True)

    parser.add_argument("-p", "--pause", help="Limit throughput by pausing between two transmissions",
                        type=float, default=0.0)

    parser.add_argument("glob", type=str,
                        help="input files to parse in a glob format")

    parser.add_argument("ais_type", type=str,
                        choices=[ais_utils.GLOBAL, ais_utils.BRITTANY],
                        default=ais_utils.GLOBAL,
                        help="ais type")

    args = parser.parse_args()

    print(args)
    bootstrap_servers = args.bootstrap_servers
    source_glob = args.glob
    ais_type = args.ais_type

    producer = KafkaProducer(
        key_serializer=lambda k: str(k).encode("utf-8"),
        value_serializer=lambda v: json.dumps(v).encode("utf-8"),
        bootstrap_servers=bootstrap_servers)
    files =  glob.glob(source_glob)
    print("Number of files to parse : %s" % len(files))
    start_time = time.time()
    current_time = time.time()
    file_count = 0
    count = 1
    for g in sorted(files):
        file_count = file_count + 1
        msgs = []
        with open(g) as f:
            print("Parsing %s", g)
            for line in f:
                msg = ais_utils.decode(line, ais_type=ais_type)
                if not msg:
                    logging.error("msg can't be decoded: %s" % line)
                    continue
                t = ""
                if msg.get("destination"):
                    _msg = clean(msg, STATIC_KEY)
                    producer.send(TOPIC_STATIC, value=_msg, key=str(msg.get("mmsi", "null")))
                elif msg.get("x") and msg.get("y"):
                    _msg = clean(msg, DYNAMIC_KEY)
                    _msg.update(fix_pos(msg))
                    producer.send(TOPIC_DYNAMIC, value=_msg, key=str(msg.get("mmsi", "null")))
                    if args.pause:
                        time.sleep(args.pause)
                # We ignore all the other message types
                count += 1
        # in seconds
        current_time = time.time()
        elapsed = current_time - start_time
        total = len(files) * elapsed / file_count
        print("Total records: %s" % count)
        print("Estimated total time: %s" % total)
        print("Estimated remaining time: %s" % (total - elapsed))
        print("Average Message rate: %s" % (float(count)/elapsed))
        producer.flush()
    # make sure to flush the buffered message on the wire
    # before proceeding
    producer.flush()
