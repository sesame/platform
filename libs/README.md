# sesame docker images

They can be used for testing purpose, on your local machine, using docker-compose.

The `launch.sh` scrip is a wrapper that allow to start/build/push a subset of the docker image.
This allow to test specific scenarios.

For instance:

```
./launch.sh up --remove-orphans zookeeper kafka ingestion multitaskais_stream
```