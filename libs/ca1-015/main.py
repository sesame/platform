#!/usr/bin/env python
"""
Sends an alert when a ship is in a EEZ
"""

import argparse

from sesamelib.sesame_faust import MutatedDynamicMessage

import faust


APP_NAME = "ca1-015"


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Alert if a ship enters one of the indexed EEZs")
    parser.add_argument("--bootstrap_servers", "-b",
                        help="Kafka bootstrap servers",
                        action="append",
                        required=True)
    args = parser.parse_args()

    app = faust.App(
        APP_NAME,
        broker="kafka://{}".format(",".join(args.bootstrap_servers))
    )

    topic = app.topic("ais.dynamic.mutated", value_type=MutatedDynamicMessage)
    out_topic = app.topic(APP_NAME)

    async def debug(msg):
        print(msg)

    @app.agent(topic, sink=[out_topic, debug])
    async def mutate_all(msgs):
        async for msg in msgs:
            zd = msg.zone_distance
            if zd is None:
                continue
            zmrgid = msg.zone_mrgid
            if zd <= 0.0 and zmrgid is not None:
                yield msg

    app.finalize()
    worker = faust.Worker(app,
                          loglevel="INFO")

    worker.execute_from_commandline()
