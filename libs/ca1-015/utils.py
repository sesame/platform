from osgeo import ogr
import xml.etree.ElementTree as ET

import logging

LIMIT_INTERIOR = 10

# NOTE(msimonin): Python is a bit conservative with the recursion level
# (And I don't want to rewrite in a imperative way...)
import sys
sys.setrecursionlimit(100000)


def points_geometry(geometry):
    """
    recursively descend until we find a GetPoints method applicable

    A geometry in our context is a multisurface. A surface is a connex part of
    the eez composed of an exterior polygon and many interior polygons. E.G:
    http://www.marineregions.org/gazetteer.php?p=details&id=5676 is composed of
    one exterior polygons an more than 17K interior polygons representing the
    islands
    """
    if geometry.GetPoints() is not None:
        return [geometry.GetPoints()]
    else:
        count = geometry.GetGeometryCount()
        points = []
        for idx in range(count):
            pts = points_geometry(geometry.GetGeometryRef(idx))
            points.extend(pts)
    return points


def update_not_none(geometry, key, value):
    if value is None:
        geometry.update({key: "No value for %s" % key})
    else:
        geometry.update({key: value.text})


def load_geometry(gml_path, only_exteriors=False, dTolerance=None):
    """
    Load the Geometry from a gml file.

    Returns a dict:
    {
      "name": name of the eez (str)
      "eez" : file path to the gml file (str)
      "geometry": ogr.Geometry
      "points": points that compose the geometry (list of list)
    }
    """

    tree = ET.parse(gml_path)
    root = tree.getroot()

    name =  root.find(".//{geo.vliz.be/MarineRegions}geoname")
    mrgid =  root.find(".//{geo.vliz.be/MarineRegions}mrgid")

    geometry = {"eez": gml_path}

    update_not_none(geometry, "name", name)
    update_not_none(geometry, "mrgid", mrgid)

    multisurface = root.findall(".//{http://www.opengis.net/gml}MultiSurface")

    if len(multisurface) != 1:
        logging.error("[%s] We found %s multisurface" % (gml_path, len(multisurface)))
        return None

    geometry.update({"type": "full"})
    if only_exteriors:
        # we remove all the interiors polygons first
        polygons = root.findall(".//{http://www.opengis.net/gml}Polygon")
        for p in polygons:
            interiors = p.findall(".//{http://www.opengis.net/gml}interior")
            for interior in interiors:
                p.remove(interior)
        geometry.update({"type": "only_exteriors"})

    geom = ogr.CreateGeometryFromGML(ET.tostring(multisurface[0], encoding="unicode"))

    if dTolerance is not None:
        geom = geom.SimplifyPreserveTopology(dTolerance)
        geometry.update({"type": "simplified-%s" % dTolerance})


    geometry.update({
        "geometry": geom
    })

    # NOTE(msimonin): we treat the first levels differently as it contains all
    # the exteriors.
    for idx in range(geom.GetGeometryCount()):
        geometry.setdefault("points", [])
        geometry["points"].append(points_geometry(geom.GetGeometryRef(idx)))

    return geometry


def plot_zone(geometry):
    """ Draw lines corresponding to the zone. The exterior lines are plotted
    first, the number of interior lines are limited to LIMIT_INTERIOR """

    import matplotlib.pyplot as plt

    for lines in geometry["points"]:
        for line in lines[0:LIMIT_INTERIOR]:
            # plotting
            x = [x for x,_ in line]
            y = [y for _,y in line]
            # NOTE(msimonin): inverting x et y for plotting
            plt.plot(y, x)
            plt.title("%s - %s" % (geometry["name"], geometry["mrgid"]))
    return plt
