#!/usr/bin/env python
"""
Mutate the original AIS with some minimal pre-computed informations.
"""

import argparse
import glob
import logging
import pickle

from sesamelib.geometry import IndexedGeometry, distance
from sesamelib.sesame_faust import BaseDynamicMessage

import faust

logging.basicConfig(level=logging.INFO)

APP_NAME = "ca1-015-csquares-%s"

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Mutate the ais dynamic signal")
    parser.add_argument("--bootstrap_servers", "-b",
                        help="Kafka bootstrap servers",
                        action="append",
                        required=True)
    parser.add_argument("-c", "--cell_size", help="cell_size to use. Will look in cell_size subdirectory to load the geometries",
                        type=float, default=0.0)

    args = parser.parse_args()

    logging.info(args)

    files = glob.glob("/data/%s/*.pickle" % args.cell_size)
    # will hold all the indexed geometries
    geometries = []
    for f in files:
        with open(f, "br") as f:
            geometry = pickle.load(f)
            geometries.append(geometry)

    logging.info("Loaded %s geometries" % len(geometries))

    app = faust.App(
        APP_NAME % args.cell_size,
        broker="kafka://{}".format(",".join(args.bootstrap_servers))
    )

    topic = app.topic("ais.dynamic",
                      key_type=str,
                      value_type=BaseDynamicMessage)
    out_topic = app.topic(APP_NAME % args.cell_size)

    pmsgs = {}

    @app.agent(topic)
    async def ca1_15(msgs):
        async for msg in msgs:
            # because of AIS, or, me, maybe
            x = msg.y
            y = msg.x
            for geometry in geometries:
                d = geometry.distance(x, y)
                if d <= 0.0:
                    print("%s[%s] -> %s" % (geometry.name, geometry.mrgid, msg))
                    await out_topic.send(key=str(msg.mmsi), value=msg)

    app.finalize()
    worker = faust.Worker(app,
                          loglevel="INFO")
    worker.execute_from_commandline()
