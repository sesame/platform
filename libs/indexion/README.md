# Use the published docker image

vagrant setup

```
docker run \
  -ti msimonin/sesame_indexion \
  --url 192.168.143.244:9200 \
  --bootstrap_server 192.168.143.245:29092
```

> You can add more `indexer` to speed up the indexing process (up to the number
  of partitions of the ais topic)
