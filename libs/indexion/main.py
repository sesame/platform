#!/usr/bin/env python
"""
Index the kafka ais Messages to ElasticSearch

Input: ais topic of Kafka
Output: index ais (dynamic) into ElasticSearch
"""
import argparse
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
import json
from kafka import KafkaConsumer

TOPIC= "ais.dynamic.mutated"

PROPERTIES = {
    "mmsi": {"type": "keyword"},
    "sog": {"type": "double"},
    "true_heading": {"type": "double"},
    "x": {"type": "double"},
    "y": {"type": "double"},
    "tagblock_timestamp": {"type": "double"},
    "d0x": {"type": "double"},
    "d0y": {"type": "double"},
    "d0t": {"type": "double"},
    "zone_mrgid": {"type": "keyword"},
    "zone_distance": {"type": "double"},
    "timestamp": {"type": "date"},
    # generated one
    "location": {"type": "geo_point"},
}

BODY = {
    "mappings": {
        "ais_dynamic": {
            "properties": PROPERTIES
        }
    }
}

# We'll remove any extra field
authorized_fields = PROPERTIES.keys()

def gen_ais(consumer, index_name, type_name):
    for msg in consumer:
        ais = msg.value
        if not ais.get("x"):
            continue
        # remove reserved keyword of any
        for f in list(ais.keys()):
            if f not in authorized_fields:
                ais.pop(f, None)
        # Let's correct some lat/lon mistakes ...
        # and save the original ones
        if ais["x"] > 180 or ais["x"] < -180 :
            ais["_x_old"] = ais["x"]
            ais["x"] = ais["x"] % 180
        if ais["y"] > 90 or ais["y"] < -90 :
            ais["_y_old"] = ais["y"]
            ais["y"] = ais["y"] % 90

        ais.update({
            "_index": index_name,
            "_type": type_name,
            "location": [ais.get("x"), ais.get("y")],
        })
        yield ais

if __name__ == '__main__':
    index_name = "ais-2017.03"
    type_name = "ais_dynamic"

    parser = argparse.ArgumentParser(description="Index ais topic to ElasticSearch")
    parser.add_argument('--bootstrap_servers', '-b',
                        help="Kafka bootstrap_servers",
                        action="append",
                        required=True)

    parser.add_argument('--urls', '-u',
                        help="Elasticsearch url",
                        action="append",
                        required=True)

    parser.add_argument('--chunk_size', '-c',
                        help="Chunk size for Elasticsearch",
                        default=500)

    args = parser.parse_args()
    print(args)

    # TODO(msimonin): check if it exists
    # For now you can simply launch
    # http --auth elastic:changeme DELETE http://192.168.143.244:9200/ais-2017.03
    # If you want to reindex everything from scratch
    es = Elasticsearch(args.urls)
    if not es.indices.exists(index_name):
        es.indices.create(index=index_name, body=BODY)

    consumer = KafkaConsumer(TOPIC,
                             bootstrap_servers=args.bootstrap_servers,
                             group_id="sesame.indexion2",
                             value_deserializer=lambda v: json.loads(v.decode("utf-8")),
                             auto_offset_reset = "earliest"
                             )
    count = 0
    for ok, result in streaming_bulk(
                        es,
                        gen_ais(consumer, index_name, type_name),
                        chunk_size=args.chunk_size):

        action, result = result.popitem()
        doc_id = '/%s/%s/%s' % (index_name, type_name, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        count += 1
        if not count % args.chunk_size :
            print("Indexed %s docs" % count)
