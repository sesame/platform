from __future__ import print_function

import argparse
import doctest
from functools import partial
import io
from itertools import groupby
import json
import math
from operator import itemgetter
import os
from pyspark.sql import SparkSession
import time
import pathlib2 as pathlib
import traceback


MIN_POINTS = 30
MAX_SPEED = 30
MIN_SECONDS_GAP = 4
MAX_SECONDS_GAP = 6 * 3600

OUTPUT_DIR = "/tmp/trajectories"

# ais types
GLOBAL = "global"
BRITTANY = "brittany"

KEYS=["mmsi", "sog", "x", "y", "tagblock_timestamp", "true_heading"]

def decode(ais_type, e):
    """Decode the message according the the ais_type."""
    def _decode(raw):
        import ais.stream
        f = io.StringIO(raw)
        msg = None
        for msg in ais.stream.decode(f):
            pass
        f.close()
        return msg
    msg = {}
    try:
        if ais_type == GLOBAL:
            msg = _decode(e)
        elif ais_type == BRITTANY:
            day, hour, _e = e.split(" ")
            msg = _decode(_e)
            if msg:
                tagblock_timestamp = time.mktime(time.strptime("%s %s" % (day, hour), "%Y/%m/%d %H:%M:%S"))
                msg.update({"tagblock_timestamp": tagblock_timestamp})
        else:
            raise Exception("ais_type not recognized")
    except:
        print(e)
        traceback.print_exc()
        with open("decoding_errors.txt", "w") as f:
            f.write(e)
    return msg


def filterMandatoryKeys(msg):
    """Filter message based on mandatory keys."""
    if not msg:
        return False
    bools = [msg.get(key) for key in KEYS]
    return reduce(lambda x, y: x and y, bools, True)


def filterTrueHeading(msg):
    return msg["true_heading"] != 511


def extractKeys(msg):
    """Extract mandatory keys. Run after filter1."""
    r = {}
    for key in KEYS:
        r.update({key: msg.get(key)})
    return r


def sortTimestamps(mmsi_msg):
    """Sort the msgs by timestamp (after a group by mmsi)."""
    return mmsi_msg[0], sorted(list(mmsi_msg[1]), key=itemgetter("tagblock_timestamp"))


def enoughPoints(min_points, mmsi_msg):
    """Keeps only trajectories made of a minimal number of points."""
    # NOTE(msimonin) we maybe don't need to coerce this as a list (need to check)
    return len(list(mmsi_msg[1])) > min_points


def filterSpeed(max_speed, mmsi_msg):
    """Filter out based on speed."""

    mmsi, msgs = mmsi_msg[0], list(mmsi_msg[1])

    # Speed below MAX_SPEED
    msgs = [msg for msg in msgs if msg.get("sog") <= max_speed]
    return mmsi, msgs


def subSample(min_seconds_gap, mmsi_msg):
    """Sub-Sample the list of message.

    Keep an minimal interval of time between two consecutive point.
    1,2,3,4 -> 1,3 if min_second_gap = 1
    """
    mmsi, msgs = mmsi_msg[0], list(mmsi_msg[1])
    keep_msgs = []
    last_ts = 0
    for msg in msgs:
        current = int(msg.get("tagblock_timestamp"))
        if (current - last_ts) > min_seconds_gap:
            keep_msgs.append(msg)
            last_ts = current

    return mmsi, keep_msgs


def splitTrajectories(max_seconds_gap, mmsi_msgs):
    """Split a trajectory based on the time difference of two consecutive points.

    1,2,5,6 -> [1, 2], [5, 6] if max_hours_gap = 2
    """
    mmsi, msgs = mmsi_msgs[0], list(mmsi_msgs[1])
    split_msgs = []
    current_msgs = []
    # NOTE: there is at least one point (filtered previously)
    if len(msgs) < 1:
        return [mmsi_msgs]
    # init
    msg = msgs[0]
    last_ts = int(msg.get("tagblock_timestamp"))
    for msg in msgs:
        current = int(msg.get("tagblock_timestamp"))
        if (current - last_ts) > max_seconds_gap:
            split_msgs.append(("%s-%s" % (mmsi, len(split_msgs)), current_msgs))
            current_msgs = [msg]
        else:
            current_msgs.append(msg)
        last_ts = current

    # There are maybe still some msgs in flight
    split_msgs.append(("%s-%s" % (mmsi, len(split_msgs)), current_msgs))

    # [(mmsi-0, t1), (mmsi-1, t2)] where ti  is a list of points
    # this needs to be flatten (flatMap)
    return split_msgs


def removeDuplicate(mmsi_msgs):
    """Remove consecutive duplicated message if the position is the same.

    >>> import pprint as pp
    >>> pp.pprint(removeDuplicate((1, [{"x": 1, "y":1}])))
    (1, [{'x': 1, 'y': 1}])

    >>> pp.pprint(removeDuplicate((1, [{"x": 1, "y":1}, {"x": 1, "y": 1}])))
    (1, [{'x': 1, 'y': 1}])

    >>> pp.pprint(removeDuplicate((1, [{"x": 1, "y":1}, {"x": 1, "y": 1}, {"x": 2, "y": 1}])))
    (1, [{'x': 1, 'y': 1}, {'x': 2, 'y': 1}])

    >>> pp.pprint(removeDuplicate((1, [{"x": 1, "y":1}, {"x": 1, "y": 1}, {"x": 2, "y": 1}, {"x": 1, "y":1}])))
    (1, [{'x': 1, 'y': 1}, {'x': 2, 'y': 1}, {'x': 1, 'y': 1}])

    """
    mmsi, msgs = mmsi_msgs[0], list(mmsi_msgs[1])
    idx = 0
    msg = msgs[0]
    _msgs = [msg]
    while idx < len(msgs):
        x, y = msg["x"], msg["y"]
        while idx < len(msgs) and msg["x"] == x and msg["y"] == y:
            msg = msgs[idx]
            idx = idx + 1
        if not (msg["x"] == x and msg["y"] == y):
            _msgs.append(msg)
    return mmsi, _msgs


def addCartesianSpeed(mmsi_msgs):
    _, msgs = mmsi_msgs[0], list(mmsi_msgs[1])
    for msg in msgs:
        sog = float(msg.get("sog"))
        heading = float(msg.get("true_heading"))
        msg.update({"vx": sog * math.cos(heading * math.pi / 180.0)})
        msg.update({"vy": sog * math.sin(heading * math.pi / 180.0)})
    return mmsi_msgs


def dumpFiles(output_dir, mmsi_msg):
    """Dump the msgs (of a given mmsi) to a file.

    Side effect ahead!"""
    mmsi, msgs = mmsi_msg[0], list(mmsi_msg[1])
    path = os.path.join(output_dir, "%s.txt" % mmsi)
    with open(path, "w") as f:
        f.write(json.dumps(msgs))

    return mmsi_msg

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Build a set of trajectories from the raw ais signals")
    parser.add_argument("glob", type=str,
                        help="input files to parse in a glob format")

    parser.add_argument("ais_type", type=str,
                        choices=[GLOBAL, BRITTANY],
                        default="global",
                        help="ais type")

    # This will require to parially applied the filtered functions
    parser.add_argument('--output_dir', '-o', type=str,
                        help="Output dir",
                        default=OUTPUT_DIR)


    parser.add_argument('--min_points', '-m', type=int,
                       help="Minimal number of points in a trajectory",
                       default=MIN_POINTS)

    parser.add_argument('--max_speed', '-M', type=int,
                        help="Maximum speed allowed (message is dropped if sog > max_speed)",
                        default=MAX_SPEED)

    parser.add_argument('--min_seconds_gap', '-g', type=int,
                        help="Minimal duration between two consecutive points",
                        default=MIN_SECONDS_GAP)

    parser.add_argument('--max_seconds_gap', '-G', type=int,
                       help="Maximal duration between two consecutive points (a new trajectory is created otherwise)",
                       default=MAX_SECONDS_GAP)

    args = parser.parse_args()
    print(args)

    pathlib.Path(args.output_dir).mkdir(parents=True, exist_ok=True)

    spark = SparkSession\
        .builder\
        .appName("count")\
        .getOrCreate()
    sc = spark.sparkContext

    rdd_ais = sc.textFile(args.glob)

    a = rdd_ais.map(partial(decode, args.ais_type))\
            .filter(filterMandatoryKeys)\
            .filter(filterTrueHeading)\
            .map(extractKeys)\
            .map(lambda msg: (msg.get("mmsi"), msg))\
            .groupByKey()\
            .filter(partial(enoughPoints, args.min_points))\
            .map(sortTimestamps)\
            .map(partial(filterSpeed, args.max_speed))\
            .map(partial(subSample, args.min_seconds_gap))\
            .flatMap(partial(splitTrajectories, args.max_seconds_gap))\
            .filter(partial(enoughPoints, args.min_points))\
            .map(removeDuplicate)\
            .filter(partial(enoughPoints, args.min_points))\
            .map(addCartesianSpeed)\
            .map(partial(dumpFiles, args.output_dir))\
            .count() # force execution

    print(a)

    spark.stop()
