#!/usr/bin/env python
"""
Sends an alert when a ship stops multiple time close to an EEZ
"""

import argparse

from sesamelib.sesame_faust import MutatedDynamicMessage

import faust


APP_NAME = "ca3-015"

ZONE_THRESHOLD = 0.1
SPEED_THRESHOLD = 0.01

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Alert if a ship enters one of the indexed EEZs")
    parser.add_argument("--bootstrap_servers", "-b",
                        help="Kafka bootstrap servers",
                        action="append",
                        required=True)
    args = parser.parse_args()

    app = faust.App(
        APP_NAME,
        broker="kafka://{}".format(",".join(args.bootstrap_servers))
    )

    topic = app.topic("ais.dynamic.mutated", value_type=MutatedDynamicMessage)
    out_topic = app.topic(APP_NAME)


    pds = {}

    async def debug(msg):
        print(msg)
        print(pds.get(msg.mmsi))

    @app.agent(topic, sink=[out_topic, debug])
    async def mutate_all(msgs):
        async for msg in msgs:
            zd = msg.zone_distance
            if zd is None or zd > ZONE_THRESHOLD:
                continue

            zmrgid = msg.zone_mrgid
            if zmrgid is None:
                continue

            d0x = msg.d0x
            if d0x is None:
                continue

            d0y = msg.d0y
            if d0y is None:
                continue

            d0t = msg.d0t
            if d0t is None:
                continue

            # Get previous stuffs
            # this is prefixed with p
            pds.setdefault(msg.mmsi, {"pd0x": d0x, "pd0y": d0y, "pd0t": d0t})
            pd = pds[msg.mmsi]
            pd0x = pd.get("pd0x")
            pd0y = pd.get("pd0y")
            pd0t = pd.get("pd0t")

            pd.update(pd0x=d0x, pd0y=d0y, pd0t=d0t)

            if d0t <= 0 or pd0t <= 0:
                continue

            v0x = d0x / d0t
            pv0x = pd0x / pd0t

            v0y = d0y / d0t
            pv0y = pd0y / pd0t

            v02 = v0x**2 + v0y**2
            pv02 = pv0x**2 + pv0y**2

            if v02 < SPEED_THRESHOLD and pv02 >= SPEED_THRESHOLD:
                yield msg

    app.finalize()
    worker = faust.Worker(app,
                          loglevel="INFO")

    worker.execute_from_commandline()
