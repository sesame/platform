#!/usr/bin/env bash

set -x

# UNUSED: The namespace to use, we should condider switching to gitlab
NAMESPACE=registry.gitlab.inria.fr/sesame/platform
# FLAGS=--no-cache
FLAGS="--network host"

TARGET_REF=1.0.14

NAMESPACE=${NAMESPACE} TARGET_REF=${TARGET_REF} docker-compose -f docker-compose.yml ${@}
