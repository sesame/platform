#!/bin/bash

# This will add a local user with uid/gid passed from the environment variable
# SESAME_UID: uid to use
# SESAME_GID: gid to use
# This entry point is primarily done to give the container permission to the
# sesame data on G5K (the use needs to be on the right sesame group)
if [[ ! -z "$SESAME_UID" && ! -z "$SESAME_GID" ]]
then
    echo "Starting using UID=$SESAME_UID" and GID="$SESAME_GID"
    groupadd sesame -g $SESAME_GID
    useradd --shell /bin/bash -u $SESAME_UID -g $SESAME_GID -o -c "" -m sesame
    echo "Changing permission on /opt/prog"
    chown sesame:sesame -R /opt/prog || true
    sync
    # https://github.com/tianon/gosu
    exec gosu sesame "$@"
else
    exec "$@"
fi
