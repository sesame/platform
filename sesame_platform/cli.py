import click
import logging
import yaml

import enos_kubernetes.tasks as ekt
import sesame_platform.tasks as t
from sesame_platform.constants import CONF

logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


def load_config(file_path):
    """
    Read configuration from a file in YAML format.
    :param file_path: Path of the configuration file.
    :return:
    """
    with open(file_path) as f:
        configuration = yaml.safe_load(f)
    return configuration


#
# The following mostly wraps enos_kubernetes
# See at the end for the sesame platform sauce
#

@cli.command(help="Claim resources on Grid'5000 (frontend).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def g5k(force, conf, env):
    config = load_config(conf)
    ekt.g5k(config, force, env=env)
    # make sure the sesame storage is ready
    t._g5k_autofs(env=env)


@cli.command(help="Claim resources on vagrant (localhost).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def vagrant(force, conf, env):
    config = load_config(conf)
    ekt.vagrant(config, force, env=env)


@cli.command(help="Generate the Ansible inventory [after g5k or vagrant].")
@click.option("--env",
              help="alternative environment directory")
def inventory(env):
    ekt.inventory(env=env)


@cli.command(help="Configure available resources [after deploy, inventory or\
             destroy].")
@click.option("--env",
              help="alternative environment directory")
def prepare(env):
    ekt.prepare(env=env)


@cli.command(help="Post-install of the kubernetes cluster")
@click.option("--env",
              help="alternative environment directory")
def post_install(env):
    ekt.post_install(env=env)



@cli.command(help="Some hints about the kubernets cluster (dashboard...)")
@click.option("--env",
              help="alternative environment directory")
def hints(env):
    ekt.hints(env=env)
    print("Create the docker registry secret")


@cli.command(help="Backup the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def backup(env):
    ekt.backup(env=env)

@cli.command(help="Destroy the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def destroy(env):
    ekt.destroy(env=env)


@cli.command(help="Resets Kubernetes (see kspray doc)")
@click.option("--env",
              help="alternative environment directory")
def reset(env):
    ekt.reset(env=env)


#
# Specific to the sesame platform
#
@cli.command(help="Configure available resources [after deploy, inventory or\
             destroy].")
@click.option("--env",
              help="alternative environment directory")
def platform(env):
    t.platform(env=env)


@cli.command(help="Claim resources from a PROVIDER and configure them.")
@click.argument("provider")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def deploy(provider, force, conf, env):
    config = load_config(conf)
    ekt.PROVIDERS[provider](config, force, env=env)
    if provider == "g5k":
        t._g5k_autofs(env=env)
    ekt.inventory(env=env)
    ekt.prepare(env=env)
    ekt.post_install(env=env)
    t.platform(env=env)
    ekt.hints(env=env)
