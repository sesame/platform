from enoslib.api import generate_inventory, run_ansible, play_on
from enoslib.task import enostask
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.g5k_api_utils import get_api_username
from enoslib.infra.enos_vagrant.provider import Enos_vagrant
import logging
import os

from sesame_platform.constants import ANSIBLE_DIR

logger = logging.getLogger(__name__)


@enostask()
def platform(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "deploy"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "platform.yml")],
                env["inventory"],
                extra_vars=extra_vars)


@enostask()
def _g5k_autofs(env=None):
    assert(env is not None)
    roles = env["roles"]
    # This only work the first time...
    with play_on(roles=roles, on_error_continue=True) as p:
        p.shell("ls /srv/storage/sesame@storage1.rennes",
                become=True,
                become_user=get_api_username())