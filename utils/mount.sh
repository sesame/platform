#!/usr/bin/env bash
#
#
# This mounts the datas
#  for rw access remove the option  '-o ro'
#

sudo-g5k mkdir -p /bigdata
sudo-g5k mount -t nfs -o nfsvers=3 -o ro srv-bigdata.rennes.grid5000.fr:/srv/bigdata /bigdata
