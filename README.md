# Sesame platform

This project installs the platform to experiment in the context of the Sesame
project.
The platform can be deployed on your local machine or on Grid'5000.
On Grid'5000 you'll have access to the whole dataset (1 month of global AIS
data).
The platform can serve as a validation platform (most probably on Grid'5000) and
a development platform.

* [Use the platform](#use-the-platform)
* [Develop with the platform](#develop-with-the-platform)

# Use the platform

## Install dependency

### On your local machine

You need:

* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

Note that since the switch to kubernetes, vagrant is'nt actively maintained.

### On Grid'5000

If virtualenv isn't present :
```
pip install virtualenv --user
export PATH=$PATH:~/.local/bin
```

### Common steps to both methods

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -e .
```

## Deploy the platform

### On Grid'5000

```
pl deploy g5k
```

### On vagrant

```
pl deploy vagrant
```

### All commands

```
pl --help
```

# Using Kubernetes

TODO

The following will give you some hint on the various web uis available.

```
pl hints
```

Launch some sesame deployments/pods ...

```
# On the master
kubectl apply -f config/
```

## Check Kafka

```
# create the kafka test pod
kubectl apply -f 05_kafka_debug.yml

# Connect to it
kubectl -n sesame exec -ti testclient bash

# Start to explore kafka
root@testclient:/opt/kafka# bin/kafka-topics.sh --zookeeper sesame-kafka-zookeeper.sesame:2181 --list
__confluent.support.metrics
ais.alert
ais.dynamic
ais.static

root@testclient:/opt/kafka# bin/kafka-topics.sh --zookeeper sesame-kafka-zookeeper.sesame:2181 --describe --topic ais.dynamic | head
Topic:ais.dynamic       PartitionCount:1000     ReplicationFactor:1     Configs:
        Topic: ais.dynamic      Partition: 0    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 1    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 2    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 3    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 4    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 5    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 6    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 7    Leader: 0       Replicas: 0     Isr: 0
        Topic: ais.dynamic      Partition: 8    Leader: 0       Replicas: 0     Isr: 0

bin/kafka-console-consumer.sh --bootstrap-server sesame-kafka.sesame:9092 --topic ais.alert --property print.key=true --from-beginning
```


